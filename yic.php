<?php
/**
 * Plugin Name:   Yic Idea
 * Plugin URI:    http://www.techexactly.com
 * Description:   Adds you idea here and post it for others...
 * Version:       1.0
 * Author:        kusum
 * Author URI:    http://www.techexactly.com
 */
 
if ( ! defined( 'ABSPATH' ) ) 
{
	exit; // Exit if accessed directly.
}

define("IDEA_PLUGIN_PATH", WP_PLUGIN_PATH ."/yic");
define("IDEA_PLUGIN_URL", WP_PLUGIN_URL ."/yic");
define("IDEA_ASSETS_URL", WP_PLUGIN_URL ."/yic/assets");

global $wpdb;
require_once( ABSPATH . "wp-includes/pluggable.php" );
require_once('app/yic_loader.php');


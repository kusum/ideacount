<?php
/**
 * This file will load all the PHP files related to the application. Include all the necessary files, css and js.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
include_once('actions/scripts_loader.php');
include_once('actions/create_widget.php');
include_once('actions/create_shortcodes.php');
include_once('sql/create_dynamic_page.php');
include_once('sql/create_dbTables.php');

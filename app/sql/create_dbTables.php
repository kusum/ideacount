<?php
/**
 * This file will create two dynamic table in database. one is yic_idea_status_activity and second is yic_user_idea_activity
 */
 
 /*yic_idea_status_activity table is for checking individual idea status and totatl activitiy against individual ideas named yic_idea_status_activity*/
$create_table_1="CREATE TABLE IF NOT EXISTS yic_idea_status_activity (
  id bigint(20) UNSIGNED NOT NULL,
  post_id bigint(20) NOT NULL,
  idea_status varchar(20) NOT NULL,
  total_activity bigint(20) NOT NULL COMMENT 'Sum(Votes,Comments)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

$wpdb->query($create_table_1);

$subfunc1="ALTER TABLE yic_idea_status_activity
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY post_id (post_id),
  ADD KEY idea_status (idea_status)";
$wpdb->query($subfunc1);


$subfunc2="ALTER TABLE yic_idea_status_activity
  MODIFY id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;";
$wpdb->query($subfunc2);




/*yic_user_idea_activity table is for checking individual user activity against individual idea. this check wather a user votes to an idea or not or wather a user follow an idea or not*/
$create_table_2="CREATE TABLE IF NOT EXISTS yic_user_idea_activity (
  id int(11) NOT NULL,
  user_id bigint(20) NOT NULL,
  post_id bigint(20) UNSIGNED NOT NULL,
  post_follow tinyint(1) NOT NULL,
  post_vote int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

$wpdb->query($create_table_2);

$tab2func1="ALTER TABLE yic_user_idea_activity
  ADD PRIMARY KEY (id),
  ADD KEY user_id (user_id)";
$wpdb->query($tab2func1);


$tab2func2="ALTER TABLE yic_user_idea_activity
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2";
$wpdb->query($tab2func2);
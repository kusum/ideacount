 <div class="row">
                        <div class="col-xs-12">
                            <h3><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?php echo $idea_query_row->post_title;?></h3>
                            <div class="clearfix"></div>
                            <a class="btn btn-primary pull-right follow_btn" data-toggle="collapse" href="#collapseExample">Follow</a>

                            <div class="form-group">
                                <h5>Created <?php echo $idea_query_row->post_date;?> by <span class="yic_grn"><?php echo get_the_author_meta('display_name', $idea_query_row->post_author);?></span> - Last update 25-Aug-2017 at 06:22PM by <span class="yic_grn">Big Thinker</span></h5>
                            </div>
                        </div>
                    </div>
 <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <textarea cols="80" id="editor1" name="editor1" rows="10"><?php echo $idea_query_row->post_content;?></textarea>
                            </div>
                        </div>
                    </div>
 <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <h5 class="pull-left"><span class="yic_grn"><strong>Tags:</strong></span> big blue, widgetX, delivery, oil</h5>
                                <h5 class="pull-right"><span class="yic_grn"><strong>Categories:</strong></span> industry, process, color</h5>
                            </div>
                        </div>
                    </div>
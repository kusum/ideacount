<div class="col-md-4">

                    <!--Voting box start here-->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                        Voting</h3>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item"><i class="fa fa-arrow-circle-up"></i> Ideator</li>
                            <li class="list-group-item"><i class="fa fa-arrow-circle-up"></i> Big Thinker</li>
                            <li class="list-group-item"><i class="fa fa-arrow-circle-down"></i> Oil Man</li>
                            <li class="list-group-item"><i class="fa fa-arrow-circle-up"></i> Out of this world</li>
                            <li class="list-group-item"><i class="fa fa-arrow-circle-down"></i> Flying Dutchman</li>
                            <li class="list-group-item">
                                <button class="btn btn-success btn-block">See All Voting</button>
                            </li>
                        </ul>
                    </div>
                    <!--Voting box end here-->

                    <!--Moderator Options start here-->
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Moderator Options</h3>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">Change Status</li>
                            <li class="list-group-item">Change Categories</li>
                            <li class="list-group-item">Edit</li>
                            <li class="list-group-item">Change Author</li>
                            <li class="list-group-item">Allow / Disallow Comments</li>
                        </ul>
                    </div>
                    <!--Moderator Options end here-->

                </div>
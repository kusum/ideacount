<?php
/**
 * This file will create two page in admin pages menu. one is for the showing all ideas and second is for the single idea details.
 */
	
add_action( 'widgets_init', 'yic_create_idea_page' );
function yic_create_idea_page( $atts )
 {
	
	global $wpdb;
	$table_prefix=$wpdb->prefix;
	$user_id = get_current_user_id();
	$today=date('Y-m-d h:i:s');
 
/**
 * mysql query to create page to show all ideas with name Ideas.
 */
   $select_posttable = "select count(*) as number from ".$table_prefix."posts where post_title='ideas' AND post_type='page'"; 
   $count_post_2 = $wpdb->get_row($select_posttable,OBJECT);	
   if($count_post_2->number<=0)
   {
		$sql_insert_post = "INSERT INTO ".$table_prefix."posts (post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES ('".$user_id."', '".$today."', '".$today."', '[yic-all-ideas-widget]', 'Ideas', '', 'publish', 'open', 'open', '', 'ideas', '', '', '".$today."', '".$today."', '', '0', '', '0', 'page', '', '0')";
		$wpdb->query($sql_insert_post);
		$post_id = $wpdb->insert_id;
		$siteurl= home_url();
 		$guid=$siteurl."/?page_id=".$post_id; 
 		$sql_insert_post_update = "update ".$table_prefix."posts set guid='".$guid."' where ID = '".$post_id."'";
		$wpdb->query($sql_insert_post_update);
	}
 }
						 
/**
 * mysql query to create page to show detail of a idea with name Single Ideas.
 */	
add_action( 'widgets_init', 'yic_create_idea_detail_page' );
function yic_create_idea_detail_page( $atts )
 {	
 	global $wpdb;
	$table_prefix=$wpdb->prefix;
	$user_id = get_current_user_id();
	$today=date('Y-m-d h:i:s');
					 
	$select_posttable_detail = "select count(*) as number from ".$table_prefix."posts where post_title='Single Ideas' AND post_type='page'";
	$count_post_detail = $wpdb->get_row($select_posttable_detail,OBJECT);	
	if($count_post_detail->number<=0)
	{					 
		$sql_insert_post_single = "INSERT INTO ".$table_prefix."posts (post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES ('".$user_id."', '".$today."', '".$today."', '', 'Single Ideas', '', 'publish', 'open', 'open', '', 'single-ideas', '', '', '".$today."', '".$today."', '', '0', '', '0', 'page', '', '0')";
		$wpdb->query($sql_insert_post_single);
		$post_id_single = $wpdb->insert_id;
		$siteurl= home_url();
		$guid_single=$siteurl."/?page_id=".$post_id_single; 		
		$sql_insert_post_update_single = "update ".$table_prefix."posts set guid='".$guid_single."' where ID = '".$post_id_single."'";
		$wpdb->query($sql_insert_post_update_single);
	}
 }

/**
 * create detail view template of an idea. here while anyone click on read more of one idea, then the idea id will pass to page-single-ideas template . and then all other functions will done against that id.
 */

add_filter( 'page_template', 'idea_count_temp' );
function idea_count_temp( $page_template )
{
   if ( is_page( 'single-ideas' ) ) {
       $page_template = dirname( __FILE__ ) . '\page-single-ideas.php';
   }
   return $page_template;
}


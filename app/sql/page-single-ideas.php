<?php session_start();
get_header(); 
$user_detail = wp_get_current_user(); 
$user_id=$user_detail->ID;
$user_name=$user_detail->display_name;
$user_email=$user_detail->user_email; 
$idea_id=$_REQUEST['idea-id'];
$today=date('Y-m-d h:i:s a');
$idea_query = "SELECT * FROM ".$table_prefix."posts WHERE ID='$idea_id'" ;
$idea_query_row = $wpdb->get_row($idea_query,OBJECT);


include_once('single-idea/vote-count.php');
include_once('single-idea/idea-comment.php');
?>



<div class="yic_main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="well clearfix">
                       <?php include_once('single-idea/idea-vote.php');?>
                        <div class="col-md-6">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="pull-left">Status is :</label>
                                </div>
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>
                   
                   <?php include_once('single-idea/idea-detail.php'); ?>
                   <?php include_once('single-idea/comments.php'); ?>
                </div>
                <?php include_once('single-idea/side-bar.php'); ?>
            </div>
        </div>
    </div>

 <script src="https://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
    <script type="text/javascript">
        $('.follow_btn').click(function() {
            console.log($(this).text());
            if ($(this).text().trim() == 'Follow') {
                $(this).text('Unfollow');
            } else {
                $(this).text('Follow');
            }
        });
    </script>
    <script>
        var editor1 = CKEDITOR.replace('editor1', {
            extraAllowedContent: 'div',
            height: 460
        });
        editor1.on('instanceReady', function() {
            // Output self-closing tags the HTML4 way, like <br>.
            this.dataProcessor.writer.selfClosingEnd = '>';
            var dtd = CKEDITOR.dtd;
            for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
                this.dataProcessor.writer.setRules(e, {
                    indent: true,
                    breakBeforeOpen: true,
                    breakAfterOpen: true,
                    breakBeforeClose: true,
                    breakAfterClose: true
                });
            }
        });
    </script>
<?php get_footer();?>
<?php
/**
 * This file will create a two widgets. one is for creat ideas and second is for showing recent ideas upto 10 limit. here one button is also created which will show all ideas while we click on that.
 */

class yic_create_idea_widget extends WP_Widget {
  // Set up the widget name and description.
  public function __construct() 
  {
    $widget_options = array( 'classname' => 'yic_create_idea', 'description' => 'Here you can creat your ideas' );
    parent::__construct( 'create_idea', 'Yic Create Idea', $widget_options );
  }


  // Create the widget output.
  public function widget( $args, $instance ) 
  {
    $title = apply_filters( 'widget_title', $instance[ 'title' ] );
    $blog_title = get_bloginfo( 'name' );
    $tagline = get_bloginfo( 'description' );

    echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title']; 
	 //include_once('frontend/crate_idea.php');
	echo do_shortcode('[yic-create-your-idea]');
     echo $args['after_widget'];
  }

  
  // Create the admin area widget settings form.
  public function form( $instance ) 
  {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : ''; ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">Titlggggge:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
    </p><?php
  }


  // Apply settings to the widget instance.
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
    return $instance;
  }

}

// Register the create idea form widget.
function yic_create_idea_widget_register() { 
  register_widget( 'yic_create_idea_widget' );
}
add_action( 'widgets_init', 'yic_create_idea_widget_register' );


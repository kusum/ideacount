<?php /**
 * This function will create a shortcode for create idea.
 */

add_shortcode( 'yic-create-your-idea', 'yic_create_your_idea_here' );
function yic_create_your_idea_here( $atts )
 {   
 	 //ob_start();
	 $user = wp_get_current_user();
	// print_r($user);
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	
	global $wpdb;
	$user_id = $user->ID;
	$today=date('Y-m-d h:i:s');
	$table_prefix=$wpdb->prefix;
	
	if($user_id=='' || $user_id==0)
	{
		echo $myvariable = '<div class="yic_main ">
        <div class="container col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="yic_recent_ideas">
                        <h2>Create an Idea</h2>
                        <div class="yic_wrap yic_wrap_front">
                            <div class="row">
                                <div class="col-md-12"><div class="logerr">Please login firts to post your idea</div>
								  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
	<form method="post" action="'.home_url().'/wp-login.php" class="wp-user-form">
				<div class="username">
					<label for="user_login">Username: </label>
					<input type="text" name="log" value="" size="20" id="user_login" tabindex="11" />
				</div>
				<div class="password">
					<label for="user_pass">Password:</label>
					<input type="password" name="pwd" value="" size="20" id="user_pass" tabindex="12" />
				</div>
				<div class="login_fields">
					<div class="rememberme">
						<label for="rememberme">
							<input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Remember me
						</label>
					</div>
				
					<input type="submit" name="user-submit" value="Login" tabindex="14" class="user-submit" />
					<input type="hidden" name="redirect_to" value="'.$_SERVER['REQUEST_URI'].'" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>';
								
	}
	else
	{
		if($msg=='successideacreate'){
			$createidea = '<div class="success">Created Successfully</div>';
			$msg='';
		}
    $createidea .= ' <div class="yic_main ">
        <div class="container col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="yic_recent_ideas">
                        <h2>Create an Idea</h2>
                        <div class="yic_wrap yic_wrap_front">
                            <div class="row">
                                <div class="col-md-12">
								<form action="" method="post">
                                        <div class="form-group">
                                          <label><i class="fa fa-lightbulb-o" aria-hidden="true"></i>
 Create an Idea</label>
                                          <input type="text" class="form-control" placeholder="Idea Title" name="title" required />
										  <input type="text" name="pageurl" value="'.$actual_link.'">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Template ID</label>
                                        <select class="form-control" name="template">
                                            <option value="1">Template#02</option>
                                            <option value="2">Template#03</option>
                                            <option value="3">Template#04</option>
                                        </select>
                                    	</div> 
                                        
                                        <div class="well">
                                        <div class="form-group">
                                          <label class="radio-inline"><input type="radio" value="">Use Template</label>
                                            <label class="radio-inline"><a href="">View Template</a></label>
                                        </div>
                                        <div class="form-group">
                                            <div class="text_editor">
                                            <table style="width:100%">
                                            <tr>
                                                <td id="maintab" style="width:50%;vertical-align: top">
                                                    <div id="uniqbox" class="box">
                                                        <textarea  name="content" style="height:230px"></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            </table>
                                            </div>
                                           
                                        </div>
                                        </div>
                                 
                                        <div class="form-group">
                                          <label>Tags</label>
                                          <input type="text" class="form-control" placeholder="Enter tags separated by comma" name="tags">
                                        </div>

                                        <div class="pull-left">
                                            <input type="submit" class="btn btn-success" name="publishnow" value="Publish" />
                                            <input type="cancle" class="btn btn-default" name="Cancle" value="Cancle" />
                                        </div>
                                        </form>
										  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>';
	}
	
	if(isset($_POST['publishnow']) && $_POST['publishnow']=='Publish')
		{
			extract($_POST);
			$content_real=addslashes($content);
			$tags_real=addslashes($tags);
			$tags_array=explode(",",$tags_real);
			$taglength=sizeof($tags_array);
			$slug=strtolower(str_replace(" ","-",$title));
	
	 /**
	 * this query is to add a idea in database POST table with a custome post type yic_idea. 
	 */
	        $chk_ide_exist = "select count(*) as number from  ".$table_prefix."posts where post_name = '".$slug."'";
			$number = $wpdb->get_row($chk_ide_exist);
			$ideaexist=$number->number; 
			if($ideaexist<1)
			{						
			$sql_insert_post = "INSERT INTO ".$table_prefix."posts (post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES ('".$user_id."', '".$today."', '".$today."', '".$content_real."', '".$title."', '', 'publish', 'open', 'open', '', '".$slug."', '', '', '".$today."', '".$today."', '', '0', '', '0', 'yic_idea', '', '0')";
			$wpdb->query($sql_insert_post);
			$post_id = $wpdb->insert_id;
			$siteurl= home_url();
			$guid=$siteurl."/single-ideas?idea-id=".$post_id; 
			$update_post_idea_guid = "update `".$table_prefix."posts` set `guid`='".$guid."' where ID = '".$post_id."'";
			$wpdb->query($update_post_idea_guid);
	
	 /**
	 * Those query are to add a idea meta tags in database POST META table with a custome meta key. 
	 */									
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key) VALUES ('".$post_id."', 'yic_last_activity')";
			$wpdb->query($sql_insert_postmeta1);
						 
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key) VALUES ('".$post_id."', 'yic_activity_who')";
			$wpdb->query($sql_insert_postmeta1);
							 
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key) VALUES ('".$post_id."', 'yic_activity_type')";
			$wpdb->query($sql_insert_postmeta1);
							 
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key) VALUES ('".$post_id."', 'yic_post_views')";
			$wpdb->query($sql_insert_postmeta1);
						 
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key) VALUES ('".$post_id."', 'yic_post_net_vote')";
			$wpdb->query($sql_insert_postmeta1);
							 
			$sql_insert_postmeta1 = "INSERT INTO ".$table_prefix."postmeta(post_id, meta_key, meta_value) VALUES ('".$post_id."', '_".$table_prefix."page_template', '".$template."')";
			$wpdb->query($sql_insert_postmeta1);
							 
	
									
			for($i=0;$i<$taglength;$i++)
				{			
					$title_tag=$tags_array[$i];
					$slug_tag=strtolower(str_replace(" ","-",$title_tag));
					/**
					* Those query are to add a idea  tags in database TERM table with a custome tags. 
					 */	
					$sql_insert_term = "INSERT INTO `".$table_prefix."terms` (`name`, `slug`) VALUES ('".$title_tag."', '".$slug_tag."')";
					$wpdb->query($sql_insert_term);
					
					/**
					* Those query are to add a relationship between idea and tagd in database term_relationships table . 
					*/	
										
					$sql_insert_term_relationships = "INSERT INTO `".$table_prefix."term_relationships` (`object_id`, `term_taxonomy_id`) VALUES ('".$post_id."', '".$term_id."')";
					$wpdb->query($sql_insert_term_relationships);
					/**
					* Those query are to undestand the posted tag as tag in  database term_taxonomy table  with taxonomy as post_tag. 
					*/	
										
					$sql_insert_term_taxonomy = "INSERT INTO `".$table_prefix."term_taxonomy` (`term_id`, `taxonomy`) VALUES ('".$term_id."', 'post_tag')";
					$wpdb->query($sql_insert_term_taxonomy);
				}
				/*echo "<script>window.location.href='". $pageurl."'</script>";		*/	
			}
			
		}
	
	 ob_get_clean();
    return $createidea;
   }
   
/**
 * This function will create a shortcode for list of recent idea.
 */
add_shortcode( 'yic-recent-ideas-widget', 'yic_recent_idea' );
function yic_recent_idea( $atts )
 {
	global $wpdb;
	$table_prefix=$wpdb->prefix;
    ob_start();
    $query = "SELECT ".$table_prefix."posts.* FROM ".$table_prefix."posts, ".$table_prefix."postmeta WHERE ".$table_prefix."posts.ID = ".$table_prefix."postmeta.post_id AND ".$table_prefix."posts.post_status = 'publish' AND ".$table_prefix."posts.post_type = 'yic_idea' AND ".$table_prefix."posts.post_date < NOW() group by(".$table_prefix."posts.ID) order by (".$table_prefix."posts.post_date) DESC limit 0,4";
   $recent_result = $wpdb->get_results($query,OBJECT);
	foreach($recent_result as $recent_list)
	{   $auid=$recent_list->post_author; 
	 $author = get_the_author($auid);
   echo  $recentidea= '<div class="yic_main">
            <div class="row">
                <div class="col-md-12">    
    				<div class="yic_recent_ideas">
            			<div class="row">
                                <div class="col-md-10">
                                    <div class="yic_idea_details">
                                        <h3><a href="'.home_url().'/single-ideas/?idea-id='.$recent_list->ID.'">'.$recent_list->post_title.' <span class="active_txt">( Active)</span></a></h3>
                                        <h5>Created '.$recent_list->post_date.' by <span class="yic_grn">'.$author.'</span> - Last update 25-Aug-2017 at 06:22PM by <span class="yic_grn">Big Thinker</span></h5>
                                        <p>'.$recent_list->post_content.'</p>
                                        <div class="pull-left">
                                            <i class="fa fa-comments" aria-hidden="true"></i> '.$recent_list->comment_count.'
                                        </div>
                                    </div>
                                </div>
                            </div>
       </div></div></div></div>';
     } 
	$recentidea = ob_get_clean();
    return $recentidea;
	
 }


/**
 * This function will create a shortcode for list of all idea.
 */
add_shortcode( 'yic-all-ideas-widget', 'yic_all_idea' );
function yic_all_idea( $atts )
 {
	global $wpdb;
	$table_prefix=$wpdb->prefix;
    ob_start();
    $query = "SELECT ".$table_prefix."posts.* FROM ".$table_prefix."posts, ".$table_prefix."postmeta WHERE ".$table_prefix."posts.ID = ".$table_prefix."postmeta.post_id AND ".$table_prefix."posts.post_status = 'publish' AND ".$table_prefix."posts.post_type = 'yic_idea' AND ".$table_prefix."posts.post_date < NOW() group by(".$table_prefix."posts.ID)";
   $recent_result = $wpdb->get_results($query,OBJECT);
	foreach($recent_result as $recent_list)
	{   
		//print_r($recent_list);
		$auid=$recent_list->post_author; 
		$author = get_the_author($auid);
	    echo  $idealist= '<div class="row">
                         <div class="col-md-2">
                           <div class="yic_bulb">100</div>
                         </div>
                         <div class="col-md-10">
                            <div class="yic_idea_details">
                              <h3>'.$recent_list->post_title.'  <span class="active_txt">( Active)</span></h3>
                              <h5>Created '.$recent_list->post_date.' by <span class="yic_grn">'.$author.'</span> - Last update 25-Aug-2017 at 06:22PM by <span class="yic_grn">Big Thinker</span></h5>
                               <p>'.$recent_list->post_content.'</p>
                               <div class="pull-left"> <i class="fa fa-comments" aria-hidden="true"></i> '.$recent_list->comment_count.' Comment</div>
                               <div class="pull-right">
                                  <a href="'.home_url().'/single-idea/?idea-id='.$recent_list->ID.'" class="btn btn-default">Read More..</a>
                               </div>
                              </div>
                           </div>
                       </div>';
     } 
	$idealist = ob_get_clean();
    return $idealist;
	
 }
 
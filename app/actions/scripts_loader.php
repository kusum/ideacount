<?php
//for ie compatibility include conditional js or stylesheets
add_action( 'wp_head', 'fn_conditional_scripts_loader');
function fn_conditional_scripts_loader()
{
	/*echo '
		<!--[if lt IE 9]>
			<script src="'.ADORE_WPFW_PLUGIN_URL.'/cloud_assets/js/flot/excanvas.min.js"></script>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	';*/
}


/**
 * Function to include all stylesheet and javascripts selectively in page/post.
 */
add_action( 'wp_enqueue_scripts', 'fn_enqueue_script' );
function fn_enqueue_script()
{	
	global $post;	
	$post_slug=$post->post_name;
	
	$localized_js_version='2.2';	
	
	fn_register_styles();
	fn_register_scripts();	
	
	wp_enqueue_style('bootstrap_css');
	wp_enqueue_style('bootstrap-min-css');
	wp_enqueue_style('datepicker_css');
	wp_enqueue_style('font-awesome-min_css');
	wp_enqueue_style('idea_style');
	wp_enqueue_script('bootstrap_js');
	wp_enqueue_script('bootstrap_min_js');
	wp_enqueue_script('canvasjs_min_js');
	wp_enqueue_script('datepicker_js');
	wp_enqueue_script('function_js');
	wp_enqueue_script('jquery_canvasjs_min_js');
	wp_enqueue_script('jquery_js');
			
	
}


/**
 * Function to register css
 */
function fn_register_styles()
{
	wp_register_style('bootstrap_css', IDEA_PLUGIN_URL.'/assets/css/bootstrap.css');
	wp_register_style('bootstrap-min-css', IDEA_PLUGIN_URL.'/assets/css/bootstrap.min.css');	
	wp_register_style('datepicker_css', IDEA_PLUGIN_URL.'/assets/css/datepicker.css');
	wp_register_style('font-awesome-min_css', IDEA_PLUGIN_URL.'/assets/css/font-awesome.min.css');
	wp_register_style('idea_style', IDEA_PLUGIN_URL.'/assets/css/style.css');
}

/**
 * Function to register JS
 */
function fn_register_scripts()
{
	wp_register_script('bootstrap_js', IDEA_PLUGIN_URL.'/assets/js/bootstrap.js');
    wp_register_script('bootstrap_min_js', IDEA_PLUGIN_URL.'/assets/bootstrap.min.js');	
	wp_register_script('canvasjs_min_js', IDEA_PLUGIN_URL.'/assets/canvasjs.min.js');
	wp_register_script('datepicker_js', IDEA_PLUGIN_URL.'/assets/js/datepicker.js');
    wp_register_script('function_js', IDEA_PLUGIN_URL.'/assets/function.js');	
	wp_register_script('jquery_canvasjs_min_js', IDEA_PLUGIN_URL.'/assets/jquery.canvasjs.min.js');
	wp_register_script('jquery_js', IDEA_PLUGIN_URL.'/assets/js/jquery.js');
}
?>
